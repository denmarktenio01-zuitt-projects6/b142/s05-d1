package com.tenio.b142.s02.s02app.services;

import com.tenio.b142.s02.s02app.models.User;
import com.tenio.b142.s02.s02app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImplementation implements UserService{
    @Autowired
    private UserRepository userRepositories;

    // CREATE USER
    public void createUser(User newUser) {
        userRepositories.save(newUser);
    }

    // UPDATE USER
    public void updateUser(Long id, User updatedUser) {
        User existingUser = userRepositories.findById(id).get();
        existingUser.setUsername(updatedUser.getUsername());
        existingUser.setPassword(updatedUser.getPassword());
        userRepositories.save(existingUser);

    }

    // DELETE USER
    public void deleteUser(Long id) {
        userRepositories.deleteById(id);
    }

    // GET ALL USERS
    public Iterable<User> getUsers() {
        return userRepositories.findAll();
    }


    // FIND USER BY USERNAME
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepositories.findByUsername(username));
    }


}
